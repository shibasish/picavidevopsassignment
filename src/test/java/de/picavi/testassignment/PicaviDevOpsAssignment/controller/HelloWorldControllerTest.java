package de.picavi.testassignment.PicaviDevOpsAssignment.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions.*;

public class HelloWorldControllerTest {

    @Test
    void testHelloWorld(){
        HelloWorldController helloWorldController = new HelloWorldController();

        Assertions.assertEquals(helloWorldController.printHelloWorld(), "Hello World");

    }
}
