package de.picavi.testassignment.PicaviDevOpsAssignment.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping(method = RequestMethod.GET, value = "/test")
    public String printHelloWorld(){
        return "Hello World";
    }
}
