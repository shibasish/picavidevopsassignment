package de.picavi.testassignment.PicaviDevOpsAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PicaviDevOpsAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(PicaviDevOpsAssignmentApplication.class, args);
	}

}
