FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install

FROM openjdk:11-slim
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/PicaviDevOpsAssignment-0.0.1-SNAPSHOT.jar /app/PicaviDevOpsAssignment.jar
ENTRYPOINT ["java", "-jar", "PicaviDevOpsAssignment.jar"]